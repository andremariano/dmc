(function(window)
{
    var helpers = {};

    helpers.scrollToByID = function(id, time)
    {
        time = (time==undefined) ? 100 : time;
        $('html,body').animate(
        {
            scrollTop: $("#" + id).offset().top
        }, time);
    };

    helpers.scrollToByPos = function(pos)
    {

        $('html,body').stop(true, true).animate(
        {
            scrollTop: pos
        }, 1000);
    };

    helpers.makeSlug = function(string)
    {
        var newstring = "";
        if(string)
        {
            string = string.toString();
            newstring = string.toLowerCase();
            newstring = newstring.replaceAll('+', ' ');
            newstring = $.trim(newstring);
            newstring = newstring.replaceAll(' ', '-');
            newstring = newstring.replaceAll('//', '/');
            newstring = newstring.replaceAll('.', '');
            newstring = newstring.replaceAll(',', '');
            newstring = newstring.replaceAll('?', '');
            newstring = newstring.replaceAll('!', '');
            newstring = newstring.replaceAll(':', '');
            newstring = newstring.replaceAll('"', '');
            newstring = newstring.replaceAll("'", '');
            newstring = accent_fold(newstring);

            function accent_fold(s)
            {
                var ret = '';
                var accent_map =
                {
                    'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', // a
                    'ç': 'c',                                                   // c
                    'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e',                     // e
                    'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',                     // i
                    'ñ': 'n',                                                   // n
                    'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ø': 'o', // o
                    'ß': 's',                                                   // s
                    'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u',                     // u
                    'ÿ': 'y'                                                    // y                    
                };
                if (!s) { return ''; }

                for (var i = 0; i < s.length; i++)
                    ret += accent_map[s.charAt(i)] || s.charAt(i);

                return ret;
            }
        }
        return  newstring;
    }

    window.helpers = helpers;
} (window));

var path;

(function(window)
{
    var main = {};
    main.el = {
        body: $('body'),
        header: $('header'),
        footer: $('footer')
    };

    main.currentSection = "";

    main.init = function init()
    {
        main.setEvents();
        main.onResize();
        $(window).resize(function(e)
        {
            main.onResize(e);
        });

        $(window).scroll(function(e)
        {
            main.onScroll(e);
        });

        console.log('init');


        $.getJSON("/js/data-realizacao.json", function (data) {
            console.log(data);

            var $target = $('#realizacao > .container');

            $.each(data, function (key, val) {
                var node = val;
                $('<h2>' + node.title + '</h2>').appendTo($target);

                var $row = $('<div class="row"></div>').append($member);

                for (var i = 0; i < val.members.length; i++) 
                {
                    var member = val.members[i];
                    var $img = $('<img class="logo-parceiros"/>').attr('src', '/img/logos-realizacao/' + member.image);
                    var $link = $('<a></a>').attr('href', member.url).attr('title', member.title).attr('target', '_blank').append($img);
                    var $member = $('<div class="member col-lg-2"></div>').append($link);
                    
                    $member.appendTo($row);                   
                }

                $row.appendTo($target); 
            });
        });

        path = window.location.pathname.split( '/' );
        
        if(path[1] != "" && path[1] != undefined)
        {
            if(path[2] == "" || path[2] == undefined)
                helpers.scrollToByID(path[1], 1000);            
        }        
        
        main.onScroll();

    };

    main.setEvents = function setEvents()
    {
        $('#menu-btn').off('click').on('click', function (e) 
        {
            e.preventDefault();
            openMenu();
        });

        $('#close-menu-btn').off('click').on('click', function (e) 
        {
            e.preventDefault();
            closeMenu();    
        });

        $('#menu nav a').off('click').on('click', function(e)
        {
            var section = $(this).data('section');
            var bodySection = $('body').data('section');

            e.preventDefault();
            helpers.scrollToByID($(this).data('section'), 1000);
            closeMenu();                        
        });

        $('#scroll-down').off('click').on('click', function(e)
        {
            e.preventDefault();
            helpers.scrollToByID('sobre', 1000);
        });

        $('#scroll-up-btn').off('click').on('click', function(e)
        {
            e.preventDefault();

            helpers.scrollToByPos(0);
        });

        $('#convocatoria .typeform-share.button').off('click').on('click', function(e)
        {
            e.preventDefault();
        });

        $('.close-modal, .bg-modal').off('click').on('click', function(e)
        {
            e.preventDefault();
            fechaEvento();
        });

        $('.filtro-combo').off('click').on('click', function(e)
        {
            e.preventDefault();

            $(this).toggleClass('aberto');
        });

        $('.lista-cidades li').off('click').on('click', function(e)
        {
            e.preventDefault();

            var value = '';
            filtroCidade = value = $(this).text();
            if(value == 'Todas')
            {
                value = 'Cidade';
                filtroCidade = '';
            }

            $(this).parents('.filtro-combo').find('.filtro-label span').text(value);
            filtraEventos();
        });

        $('.lista-atividade li').off('click').on('click', function(e)
        {
            e.preventDefault();

            var value = '';
            filtroAtividade = value = $(this).text();
            if(value == 'Todas')
            {
                value = 'Atividade';
                filtroAtividade = '';
            }

            $(this).parents('.filtro-combo').find('.filtro-label span').text(value);
            filtraEventos();
        });

        $('.detalhes-evento .botao-inscrevase').off('click').on('click', function(e)
        {
            if($(this).hasClass('inactive'))
                e.preventDefault();
        });

        window.onpopstate = function(e)
        {
            //console.log("popstate");
            
            helpers.scrollToByID(e.state.section, 1000);
        };
    };

    main.onScroll = function onScroll(e)
    {        
        var posTop = $('body, html').scrollTop();
        if(posTop > 800)
        {
            $('#scroll-up-btn').addClass('show');
        } else {
            $('#scroll-up-btn').removeClass('show');
        }

        if(posTop > 0 && posTop < $('#programacao').offset().top - 5)
        {
            changeSection("home");
        } else if (posTop > $('#programacao').offset().top - 5 && posTop < $('#sobre').offset().top - 5)
        {
            changeSection("programacao");
        } else if (posTop > $('#sobre').offset().top - 5 && posTop < $('#realizacao').offset().top - 5)
        {
            changeSection("sobre");
        } else if (posTop > $('#realizacao').offset().top - 5 && posTop < $('#contato').offset().top - 5) 
        {
            changeSection("realizacao");
        } else if (posTop > $('#contato').offset().top - 5)
        {
            changeSection("contato");
        }
        
    };

    main.onResize = function onResize(e)
    {

    };

    window.main = main;
} (window));

$(document).ready(function($)
{
   
    main.init();
});

function openMenu()
{
    //console.log('openMenu');
    
    $('#menu').addClass('open');

    $('#menu nav').find('a').each(function (index) 
    {
        var $a = $(this);
        window.setTimeout(function()
        {
            $a.addClass('show');
        }, index * 150);
    });
}

function closeMenu()
{
    $('#menu').removeClass('open');

    window.setTimeout(function()
    {
        $('#menu nav a').removeClass('show');
    }, 850);
}

function changeSection(section)
{
    if(main.currentSection == section)
        return;
    
    //console.log("changeSection: " + section);
    main.currentSection = section;

    $('#menu nav a').removeClass('active');
    $('#menu nav a[data-section="' + section + '"]').addClass('active');

    window.history.pushState({section: section}, null, section);
}

function makeApiCall() 
{
    var params = {
        spreadsheetId:          '19WtD4P6_1zWPL8eYtVqn0OiEdXE8JGKxskWaaPKR_zA',
        range:                  'B2:U200',
        valueRenderOption:      'FORMATTED_VALUE',
        dateTimeRenderOption:   'SERIAL_NUMBER'
    };

    var request = gapi.client.sheets.spreadsheets.values.get(params);

    request.then(function(response) 
    {
        var values = response.result.values;
        //console.log(values);
        
        if(values != undefined)
            montaEventos(values);
    }, function(reason) 
    {
        console.error('error: ' + reason.result.error.message);
    });
}

function initClient() 
{
    var API_KEY = 'AIzaSyArajoV9y48fVnC3LP8J3ZGjYzpX3x6aKo';
    var CLIENT_ID = '134454469624-6bde1ck57pnu568ljjk0v3hnisumsnqs.apps.googleusercontent.com';
    var SCOPE = 'https://www.googleapis.com/auth/spreadsheets.readonly';

    gapi.client.init({
        'apiKey': API_KEY,
        'clientId': CLIENT_ID,
        'scope': SCOPE,
        'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
    }).then(function() {
        makeApiCall();
    });
}

function handleClientLoad() {
    gapi.load('client', initClient);
}

var eventos;

function montaEventos(dataEventos)
{
    // 0 : horário                  // 10 : descrição
    // 1 : duração texto            // 11 : cidade
    // 2 : duração em h:m           // 12 : anfitrião
    // 3 : tipo                     // 13 : endereço
    // 4 : inspirador               // 14 : vagas
    // 5 : empresa                  // 15 : status
    // 6 : portfolio / site         // 16 : link/embed inscricao
    // 7 : minibio                  // 17 : link imagem evento
    // 8 : atividade                // 18 : obs
    // 9 : material atividade       // 19 : status info

    //console.log(dataEventos);
    

    eventos = dataEventos;
    var filtroCidade = [];
    var filtroAtividade = [];

    var colorSequence = ['blue', 'white', 'yellow', 'green', 'pink'];
    var colorCounter = 0;
    
    for (var i = 0; i < dataEventos.length; i++) 
    {
        var evento = dataEventos[i];
        var $evento = $('<div class="evento col-lg-4"></div>');
        $evento.attr('data-index', i);
        $evento.attr('data-color', colorSequence[colorCounter]);
        $evento.attr('data-cidade', evento[11]);
        $evento.attr('data-atividade', evento[12]);
        $evento.attr('data-slug', helpers.makeSlug(evento[8]));
        $evento.addClass(colorSequence[colorCounter]);

        $evento.append('<div class="tipo">' + evento[3] + '</div>');
        $evento.append('<h3 class="title">' + evento[8] + '</h3>');
        $evento.append('<div class="separator"></div>');
        $evento.append('<div class="palestrante">' + evento[4] + '</div>');
        $evento.append('<div class="local">' + evento[11] + '</div>');

        $('.eventos').append($evento);

        $evento.off('click').on('click', function(e)
        {
            abreEvento($(this).data('index'), $(this).data('color'));
        });

        filtroCidade.push(evento[11]);
        filtroAtividade.push(evento[12]);

        colorCounter += 1;
        if(colorCounter == colorSequence.length)
            colorCounter = 0;
    }

    filtroCidade = filtroCidade.filter(onlyUnique);
    filtroAtividade = filtroAtividade.filter(onlyUnique);

    console.log(filtroCidade);
    
    for (var i = 0; i < filtroCidade.length; i++) 
    {
        $('.lista-cidades').append('<li>' +filtroCidade[i]+ '</li>');
    }

    sortList($('.lista-cidades'));

    for (var i = 0; i < filtroAtividade.length; i++) 
    {
        $('.lista-atividade').append('<li>' +filtroAtividade[i]+ '</li>');
    }

    sortList($('.lista-atividade'));

    $('.lista-cidades').prepend('<li>Todas</li>');
    $('.lista-atividade').prepend('<li>Todas</li>');

    main.setEvents();
    
    if(path[1] == 'programacao' && path[2] != '' && path[2] != undefined)
    {
        //console.log(path[2]);
        var $eventoPath = $('.evento[data-slug="'+path[2]+'"]');        
        abreEvento($eventoPath.data('index'), $eventoPath.data('color'));
    }
}

var filtroCidade = '';
var filtroAtividade = '';

function filtraEventos()
{
    //console.log(filtroCidade, filtroAtividade);
    
    $('.evento').hide();
    if(filtroCidade != '' || filtroAtividade != '' )
    {
        $('.evento').filter(function(idx)
        {
            if(filtroCidade == '')
                return $(this).data('atividade') === filtroAtividade;

            if(filtroAtividade == '')
                return $(this).data('cidade') === filtroCidade;

            return $(this).data('cidade') === filtroCidade && $(this).data('atividade') === filtroAtividade;
        }).show();
    } else
    {
        $('.evento').show();
    }
    
}

function abreEvento(id, classColor)
{
    var evento = eventos[id];

    $('.modal-container').scrollTop(0);

    $('.info-evento').removeClass('blue').removeClass('white').removeClass('green').removeClass('pink').removeClass('yellow').addClass(classColor);

    $('.detalhes-evento .header-evento').css({'background-image': 'url(http://www.diamundialdacriatividade.com.br/img/evento/'+evento[17]+')'});
    $('.detalhes-evento .titulo-evento').text(evento[8]);
    $('.detalhes-evento .tipo-evento').text(evento[3]);
    $('.detalhes-evento .desc-evento').text(evento[10]);

    // VIDEO
    if(evento[9] != "" && evento[9] != undefined)
    {
        var videoUrl = evento[9].split('=');
        $( '<iframe class="video-evento" width="100%" height="274" src="https://www.youtube.com/embed/' + videoUrl[videoUrl.length - 1] + '?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>' ).insertAfter( ".detalhes-evento .desc-evento" );
    }
    
    $('.detalhes-evento .palestrante').text(evento[4]);
    $('.detalhes-evento .empresa').text(evento[5]);
    $('.detalhes-evento .portfolio').text(evento[6]).attr('href', evento[6]);
    $('.detalhes-evento .minibio').text(evento[7]);
    $('.detalhes-evento .cidade').text(evento[11]);
    $('.detalhes-evento .botao-inscrevase').attr('href', evento[16]);
    $('.detalhes-evento .local').text(evento[12]);
    $('.detalhes-evento .cidade').text(evento[11]);
    $('.detalhes-evento .endereco').text(evento[13]);
    $('.detalhes-evento .vagas').text(evento[14] + '');
    $('.detalhes-evento .horario').text(evento[0]);
    $('.detalhes-evento .duracao').text(evento[1]);

    $('body').addClass('no-scroll');
    $('.modal-container').addClass('show');

    if (evento[15] == 'Lotado' || evento[15] == 'Encerrado' || evento[15] == 'lotado' || evento[15] == 'encerrado')
    {
        $('.detalhes-evento .botao-inscrevase').addClass('inactive').text(evento[15]);

    }

    var eventSlug = $('.evento[data-index = "'+id+'"]').data('slug');

    window.history.pushState({evento: eventSlug, section:'programacao'}, null, '/programacao/' + eventSlug);
}

function fechaEvento()
{
    $('body').removeClass('no-scroll');
    $('.detalhes-evento .header-evento').css({'background-image': 'none'});
    $('.video-evento').remove();
    $('.modal-container').removeClass('show');
    $('.detalhes-evento .botao-inscrevase').removeClass('inactive').text('Inscreva-se');

    window.history.pushState({ section: 'programacao' }, null, '/programacao');

}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

function sortList($list)
{
    $list.children().detach().sort(function (a, b) {
        return $(a).text().localeCompare($(b).text());
    }).appendTo($list);
}

String.prototype.replaceAll = function(target, replacement)
{
    return this.split(target).join(replacement);
};