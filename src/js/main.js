(function(window)
{
    var main = {};
    main.el = {
        body: $('body'),
        header: $('header'),
        footer: $('footer')
    }

    main.init = function init()
    {
        main.onResize();
        $(window).resize(function(e)
        {
            main.onResize(e);
        });
    };

    main.setEvents = function setEvents()
    {
        //register.setEvents();
    }

    main.onScroll = function onScroll(e)
    {

    };

    main.onResize = function onResize(e)
    {

    };

    window.main = main;
} (window));

$(document).ready(function($)
{
    main.init();
    //register.init();
});
