(function(window)
{
    var home = {};
    home.el = {
        body: $('body'),
        header: $('header'),
        footer: $('footer')
    }

    home.init = function init()
    {
        home.onResize();
        $(window).resize(function(e)
        {
            home.onResize(e);
        });
    };

    home.setEvents = function setEvents()
    {
        //register.setEvents();
    }

    home.onScroll = function onScroll(e)
    {

    };

    home.onResize = function onResize(e)
    {

    };

    window.home = home;
} (window));

$(document).ready(function($)
{
    home.init();
    //register.init();
});
