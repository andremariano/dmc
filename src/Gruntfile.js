var modRewrite = require('connect-modrewrite');

module.exports = function(grunt)
{
    grunt.template.addDelimiters('handlebars-like-delimiters', '{{', '}}');

    var grunt_config =
    {
        pkg : grunt.file.readJSON('package.json'),

        watch:
        {
            css:
            {
                files: ['./css/**/*.styl'],
                tasks: ['stylus']
            }
        },

        stylus:
        {
            compile:
            {
                options:
                {
                    compress: false,
                    paths: [],
                    relativeDest: '../public/css',
                    use:[],
                    import: []
                },

                files: {
                'style.css': './css/style.styl',
                }
            }
        },        

        uglify:
        {
            all:
            {
                files:
                {
                    '../public/js/scripts.js' : ['./temp.js']
                }
            }
        },

        browserSync:
        {
            dev:
            {
                bsFiles:
                {
                    src : [
                        '../public/css/*.css',
                        '../public/js/*.js',
                        '../public/*.html'
                    ]
                },
                options:
                {
                    watchTask: true,
                    server: {
                        baseDir: "../public/",
                        middleware: [
                            modRewrite([
                                '/home /index.html [L]',
                                '/sobre /index.html [L]',
                                '/programacao/([A-Za-z0-9_-]+)?$ /index.html [QSA,L]',
                                '/programacao /index.html [L]',
                                '/realizacao /index.html [L]',
                                '/parceiros /index.html [L]',
                                '/contato /index.html [L]'
                            ])
                        ]
                    }
                }
            }
        }
    };

    grunt.initConfig(grunt_config);

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('dev', ['stylus' , 'browserSync', 'watch']);
    grunt.registerTask('prod', ['stylus' , 'concat:dist', 'uglify']);
};
